# Contributing guidelines
Hi there!  Thank you for reaching out 😸

We are looking for contributions of different kind that ranges from incidental but meaningful contributions to extended and close collaboration.

#### Things to consider:
- We are focusing in this first iteration on small centrifuge design of 15 ml.
- We are using a reference open hardware project to replicate, test, hack and improve if possible.

## Contribution options
#### 1. User feedback
- If you are a user of this equipment you can [fill in this form](https://docs.google.com/forms/d/e/1FAIpQLScyTZIfLe0GmoX6rdi7wdyzSNGVoP2hBOGGEqeAVn0CZmRDTQ/viewform) and share it with your colleagues.

#### 2. Design improvements / Feature requests
We always consider each improvement as a hypothesis that needs to be tested.

You can also point to videos, documentation and solutions that support the hypothesis/idea you have in mind. [See this example](https://www.youtube.com/watch?v=9Cuh2msd2lo) as a style guide for test setup.

- If you have ideas about centrifuge solutions features, open an issue and describe the concept/hypothesis considering the following:
  1. How this feature improves the centrifuge design considering:
  1.1. Pains you have already had in your previous experience.
  1.2. Gains your idea has.

  2. Description of the concept.
  3. Preferably describe a test case/experiment [by using the following  markdown template](https://github.com/FOSH-following-demand/Open-source-Centrifuge-for-WetLab/blob/master/R%26D/test-cards/README.md).
  - [You can find an example here:](https://github.com/FOSH-following-demand/Open-source-Centrifuge-for-WetLab/blob/master/R%26D/test-cards/Cooling-led-2.md)
  

## Contribution rules and ettiquette:

- Pushing to `master` is disabled. We suggest you make a new branch by explicitly naming the feature / bug you are fixing or adding and submit a merge request. We are not responsible for fixing merge requests. **We urge you to pull from the latest master before submitting a merge request.** 

- We also are okay if you `fork` the repository. Please ensure you pull from master before you submit a merge request when doing so.

- Please do not create arbitrary `tags`. See the list of tags already in use by the project. Please use these in your issues and commit messages / merge requests.

- Please check the `milestone` created by us and mark all commits for a particular milestone using the name we have set.

- Please check existing / closed `issues` before opening a new one.

- **Please do not use `spaces` in file names. Also avoid any special non-ASCII characters (for example: &) in file or folder names.** This is highly inconvenient and will not be accepted. We follow the naming convention of using small caps with `-` separating two words: Example: `casing-lasercut-V0.dxf`